#Installs Rstudio Server
#Sample Apache Proxy configs included
class cran::rstudio {

    include server
    #Install rstudio server
    #Install Apache and modules
    #libapache2-mod-proxy-html now included in apache2 for 16.04 machine
    package {['apache2','libxml2-dev']:
        ensure => latest
    }

    #Install site conf files, SSL cert generation to be done later
    file {'rstudio.conf':
        ensure  => file,
        path    => '/etc/apache2/sites-available/rstudio-example.conf',
        mode    => '0644',
        require => Package['apache2'],
        content => template('cran/rstudio.conf.erb'),
    }

    file {'rstudio-ssl.conf':
        ensure  => file,
        path    => '/etc/apache2/sites-available/rstudio-ssl.example.conf',
        mode    => '0644',
        require => File['rstudio.conf'],
        content => template('cran/rstudio-ssl.conf.erb'),
    }

    # Don't auto enable configurations,
    # otherwise they could clobber customization
    #exec {'a2en-rstudio':
    #    command => '/usr/sbin/a2ensite rstudio.conf',
    #    subscribe => File['rstudio.conf'],
    #}

    #exec {'a2en-rstudiossl':
    #    command => '/usr/sbin/a2ensite rstudio-ssl.conf',
    #    subscribe => File['rstudio-ssl.conf'],
    #}

    exec {'a2dis-default':
        command => '/usr/sbin/a2dissite 000-default.conf',
        #before => Exec['a2en-rstudio'],
        require => Package['apache2'],
    }

    #Enable modules: a2enmod proxy ssl
    exec {'a2enmod-proxy':
        command => '/usr/sbin/a2enmod proxy',
        require => Package['apache2'],
    }

    exec{'a2enmod-proxy-http':
        command   => '/usr/sbin/a2enmod proxy_http',
        require   => Exec['a2enmod-proxy'],
        subscribe => Exec['a2enmod-proxy'],
    }

    exec{'a2enmod-ssl':
        command => '/usr/sbin/a2enmod ssl',
        require => Package['apache2'],
        #subscribe => Exec['a2en-rstudiossl'],
    }

    #restart the apache service
    service { 'apache2':
        ensure     => running,
        name       => 'apache2',
        hasrestart => true,
        enable     => true,
        subscribe  =>[ Exec['a2enmod-proxy'],File['rstudio-ssl.conf']],
    }

    #'wget' dropped as it's in server modules and conflicts
    package{['gdebi-core','libapparmor1','libclang-dev']: ensure => present}

    exec{'getdeb':
        #TODO: md5 verify the file
        #Download the installer http://download2.rstudio.org/,
        #url subject to change
        command => '/usr/bin/wget -q https://download2.rstudio.org/server/trusty/amd64/rstudio-server-1.2.1335-amd64.deb',
        creates => 'rstudio-server-1.2.1335-amd64.deb',
        require => [Package['wget','gdebi-core','libclang-dev'],File['/etc/rstudio']],
    }

    #Rstudio refresh not automatic as that could hurt running sessions
    package { 'rstudio':
        ensure   =>  latest,
        name     =>  'rstudio-server',
        provider =>  dpkg,
        source   =>  'rstudio-server-1.2.1335-amd64.deb',
        require  => Exec['getdeb'],
    }

    file {'/etc/rstudio':
        ensure => directory,
        path   => '/etc/rstudio/',
        owner  => 'root',
    }

    file {'rserver.conf':
        ensure  => file,
        path    => '/etc/rstudio/rserver.conf',
        mode    => '0644',
        require => File['/etc/rstudio'],
        source  => ['puppet:///modules/cran/rserver.conf'],
    }

    file {'rsession.conf':
        ensure  => file,
        path    => '/etc/rstudio/rsession.conf',
        mode    => '0644',
        require => File['/etc/rstudio'],
        source  => ['puppet:///modules/cran/rsession.conf'],
    }

}
