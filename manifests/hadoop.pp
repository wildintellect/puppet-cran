class cran::hadoop{
    #This manifest installs the dependencies needed to connect R with Hadoop

    require hadoop::client
    
    cran::package{'rJava': ensure => present}
    cran::package{'Rccp': ensure => present}
    #Bummer it's not in cran
    #cran::package{'hdfs': ensure => present}
    #Download https://github.com/RevolutionAnalytics/rhdfs/blob/master/build/rhdfs_1.0.8.tar.gz?raw=true
    #unzip
    #export HADOOP_CMD=/usr/bin/hadoop
    #R CMD build pathto/pkg
    
    #Need a /etc/hadoop/core-site.xml
    

}
