#Install R,
#Standard Biogeo R packages and system dependencies to build those packages
class cran::r {
    include apt

    apt::source { 'cran':
        location          => 'https://cloud.r-project.org/bin/linux/ubuntu',
        release           => "${::lsbdistcodename}-cran35/",
        #release       => 'trusty/',
        repos             => '',
        required_packages => 'debian-keyring debian-archive-keyring',
        key               => 'E084DAB9',
        key_server        => 'keyserver.ubuntu.com',
        pin               => '1000',
        include_src       => false,
        #notify            => Exec['apt_update']
    }

    # Add PPA specifically for rjava
    apt::ppa { 'ppa:marutter/c2d4u3.5': }

    #exec { 'apt-get update':
    #    command => '/usr/bin/apt-get update',
    #    #TODO: only run if repo changes subscribe
    #}

    #exec {'javareconf':
    #    command => '/usr/bin/R CMD javareconf -e'
        # must be run before rJava
        # requries Rstudio server to be restarted after
    #}

    package { 'r-base': ensure => latest }
    package { 'r-base-dev': ensure => latest }
    package { 'build-essential': ensure => present }

    #Dependencies for packages
    package {['libgeos-dev','libproj-dev','libgdal-dev',
            'libnetcdf-dev','netcdf-bin','libcairo2-dev', 'liblwgeom-dev']:
            ensure => present
    }

    #Install latex for knitr
    package{['texlive-full']:  ensure => present
    }
    #Might as well install the command line tools too
    package {['gdal-bin','python-gdal']: ensure => installed}
    package {['libssh2-1-dev']: ensure => present}
    package {['libudunits2-dev','libudunits2-0']: ensure => present}

    #dismo (which will install dependencies sp, raster),
    #maptools, gstat,randomForest, snow


    #install r packages
    cran::package {'dismo': ensure => present}
    cran::package {'raster': ensure => present}
    cran::package {'maptools': ensure => present}
    cran::package {'gstat': ensure => present}
    cran::package {'snow': ensure => present}
    cran::package {'randomForest': ensure => present}
    cran::package {'plyr': ensure => present}
    cran::package {'RColorBrewer': ensure => present}
    cran::package {'RSQLite':  ensure => present}
    cran::package {'fields':   ensure => present}
    cran::package {'gbm':      ensure => present}
    cran::package {'RCurl':  ensure => present}
    #cran::package{'dplyr':ensure=>present}
    cran::package {'digest':   ensure => present}
    cran::package {'jsonlite': ensure => present}
    cran::package {'classInt': ensure => present}
    cran::package {'geosphere': ensure => present}
    cran::package {'caret': ensure => present}
    cran::package {'R.utils': ensure => present}
    cran::package {'corrplot': ensure => present}
    cran::package {'devtools': ensure => present}
    cran::package {'osmdata': ensure => present}
    cran::package {'git2r':
        ensure  => present,
        require => Package['libssh2-1-dev']
    }
    cran::package{'signal':ensure => present}
    cran::package{'data.table':ensure => present}
    #Depends on data.table R package
    cran::package{'splitstackshape':
        ensure       => present,
        #dependencies => true,
    }

    #External deps rgeos, rgdal, ncdf4, udunits2
    cran::package {'rgeos':
        ensure  => present,
        require => Package['libgeos-dev']
    }
    cran::package {'rgdal':
        ensure  => present,
        require => Package['libproj-dev','libgdal-dev']
    }
    cran::package {'ncdf4':
        ensure  => present,
        require => Package['libnetcdf-dev','netcdf-bin']
    }
    cran::package {'gdalUtils': ensure => present}
    cran::package {'sf':
        ensure  => present,
        require => Package['libudunits2-dev','libudunits2-0','liblwgeom-dev']
    }

    #Requires sf and many other packages that have dependencies
    cran::package {'MODIS':
        ensure  => present,
        require => Package['libcairo2-dev']
    }
    # Plant Phenology related
    cran::package {'phenopix': ensure => present}
    cran::package {'phenex': ensure => present}

    # phenorice installed with devtools by hand from bitbucket by each user
    # TODO: when package is stable install for everyone

    #External deps java
    #cran::package{'rJava':
    #    ensure  => present,
    #    require => Exec['javareconf']
    #}


    #Switched to prepackaged
    # Also might need to add this to /etc/environment  
    #LD_LIBRARY_PATH=/usr/lib/jvm/java-1.8.0-openjdk-amd64/jre/lib/amd64/server/
    package { 'r-cran-rjava':
        ensure  => latest
        #require => Apt['ppa:marutter/c2d4u3.5']
    }

    # Maxent
    # cp maxent/maxent.jar /usr/local/lib/R/site-library/dismo/java/

    #Building docs
    require server::sphinx
    cran::package {'knitr': ensure => present}
    cran::package {'rmarkdown': ensure => present}
    cran::package {'yaml': ensure => present}
    cran::package {'htmltools': ensure => present}

    #Create a /scratch to mimic Farm cluster for cross code compatibility
    file {'/scratch':
        ensure => 'directory',
        owner  => 'root',
        mode   => '1777',
    }

}

#TODO handle local issue
# quick fix is ```sudo locale-gen en_US.UTF-8```
# Use this if R gives this message on opening
#    During startup - Warning messages:
#    1: Setting LC_CTYPE failed, using 'C'
#    2: Setting LC_COLLATE failed, using 'C'
#    3: Setting LC_TIME failed, using 'C'
#    4: Setting LC_MESSAGES failed, using 'C'
#    5: Setting LC_MONETARY failed, using 'C'
#    6: Setting LC_PAPER failed, using 'C'
#    7: Setting LC_MEASUREMENT failed, using 'C'
