#Installs Shiny Server
#Apache must be configured to proxy the internal port 3838 from the shiny conf
class cran::shiny {
    #For now requires Rstudio to ensure wget and gdebi
    require cran::rstudio

    cran::package{'shiny':
        ensure => present,
        before => Package['shiny']}
    cran::package{'leaflet':ensure => present}
    cran::package{'DT':ensure => present}

    exec{'getdebShiny':
        #TODO: md5 verify the file
        #Download the installer http://download2.rstudio.org/,
        #url subject to change
        command => '/usr/bin/wget -q https://download3.rstudio.org/ubuntu-14.04/x86_64/shiny-server-1.5.9.923-amd64.deb -O /etc/rstudio/shiny-server-1.5.9.923-amd64.deb',
        creates => '/etc/rstudio/shiny-server-1.5.9.923-amd64.deb',
        #require => [Package['cran::rstudio::wget','gdebi-core']],
        require => [Package['libssl0.9.8']]
    }

    package {'libssl0.9.8':
        ensure => latest
    }

    #Install Shiny Server
    package { 'shiny':
        ensure    =>  latest,
        name      =>  'shiny-server',
        provider  =>  dpkg,
        source    =>  '/etc/rstudio/shiny-server-1.5.9.923-amd64.deb',
        require   => Exec['getdebShiny'],
    }

    file { 'shiny-server.conf':
      ensure  => 'present',
      path    => '/etc/shiny-server/shiny-server.conf',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => Package['shiny'],
      source  => ["puppet:///modules/cran/${::hostname}-shiny-server.conf"],
    }

    #TODO trigger the shiny service to restart on conf change
    #restart the apache service
    service { 'shiny-server':
        ensure     => running,
        name       => 'shiny-server',
        hasrestart => true,
        enable     => true,
        subscribe  =>[File['shiny-server.conf']],
    }

    #Really should move this and apache install to a more generic module
    package {'libapache2-mod-auth-cas':
        ensure  => latest,
        require => File['cas.conf']
    }

    file {'cas.conf':
      ensure  => 'present',
      path    => '/etc/apache2/conf-available/cas.conf',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      before  => Package['libapache2-mod-auth-cas'],
      source  => ['puppet:///modules/cran/cas.conf'],
    }

    file {'/etc/apache2/conf-enabled/cas.conf':
        ensure  => 'link',
        target  => '/etc/apache2/conf-available/cas.conf',
        require => File['cas.conf']
    }

    #This conflicts with the refresh in rstudio
    #service { 'apache2-cas':
    #    ensure     => running,
    #    name       => 'apache2',
    #    hasrestart => true,
    #    enable     => true,
    #    subscribe  =>[File['/etc/apache2/conf-enabled/cas.conf']],
    #}

}
